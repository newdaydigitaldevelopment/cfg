/* eslint-disable no-unused-vars */


// Your code goes here

// Exercise: FUNCTIONS

// Exercise: ARGUMENTS

// Passing Variables as arguments when we invoke the function

// Exercise: CONTROL FLOW

// ADDING LOGIC()

// Exercise: FUNCTION LOGIC

// OBJECTS 
// <h1>Javascript Objects and the Document Object Model</h1>
// <h2 id="headerID">I have an ID attached</h2>

const moodColors = {
    'happy' : 'purple',
    'nervous' : 'yellow',
    'calm': 'LightBlue',
    'angry': 'grey'
}

function changeBackgroundColor(color) {
    document.body.style.backgroundColor = color;
}

function changeColor(){

    let mood = prompt(`enter mood`);

    if (mood === 'nervous') {
        return changeBackgroundColor(moodColors.nervous);
    }

    if (mood === 'calm') {
        return changeBackgroundColor(moodColors.calm);
    }

    if (mood === 'happy') {
        return changeBackgroundColor(moodColors.happy);
    }

    if (mood === 'angry') {
        return changeBackgroundColor(moodColors.angry);
    }

    alert('unknown mood, please try again!');
}