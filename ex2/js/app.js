/* gamble game

Rules: 
1. draw when same dice
2. larger dice wins

*/


// gamble
let amount = 0;
function dice() {
    return Math.floor(Math.random() * 6) + 1;
}

function play(bet) {

    const computer = dice();
    const player = dice();

    alert(`computer: ${computer}, you: ${player}`);

    if (player == computer) {
        alert(`draw`);
    }
    else {
        if (player > computer) {
            alert(`you won £${bet}`);
            amount = amount + bet;
        } 
        else {
            alert(`you lost £${bet}`);
            amount = amount - bet;
        }
    }

    alert(`you have ${amount}£`);
}

function gamble() {

    if (amount < -100) {
        alert('you have lost more than 100£ and we advice you not to gamble any more. Today is not your day!');
    } else {
        var value = prompt('how much you bet?');
        play(parseInt(value));
    }
}
