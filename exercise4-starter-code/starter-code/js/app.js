
// FUNCTIONS EXERCISE

// 1. Write a function that reverses a string
function revert(itemString) {
    var revertedSTring ='';
    for (var i = itemString.length-1; i>=0; i--) {
        revertedSTring += itemString[i];
    }

    alert(`Reverted string ${revertedSTring} for ${itemString}`);
}

//revert("Hello World!");

// 2. Write a function that sorts a string in alphabetical order
function sort(itemString) {
    var letters = itemString.split('');
    for (var i = 0; i < letters.length-2; i++) {
        for (var j = i+1; j < letters.length-1; j++) {
            if(letters[i] > letters[j]){
                var aux = letters[i];
                letters[i] = letters[j];
                letters[j] = aux;
            }
        }
    }

    alert(`'${itemString}' was sorted as '${ letters.join('')}'`);
}

sort("hello world!");
// 3. Write a function that loops over an array or Strings and capitalises each one before returning the array 


// 4. Write a function that console logs the data type of the aruments


// 5. Write a function that returns the length of the longest word in a sentence
